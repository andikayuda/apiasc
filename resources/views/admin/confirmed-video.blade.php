@extends("crudbooster::admin_template")
@section("content")
<div class="card" style="background-color: white;padding : 20px;">
    <div class="card-body">

<div class="row">
    <div class="col-md-12 text-center" >
        <video controls width="200" height="400" src="{{URL::to($video->url)}}"></video>
    </div>
</div>
<div class="row">
    <div class="col-12 text-center">
        <p>Title : {{$video->title}}</p>
    </div>
</div>
<div class="row">
    <div class="col-12 text-center">
        <p>Description : {{$video->caption}}</p>
    </div>
</div>
</div>

</div>
@endsection