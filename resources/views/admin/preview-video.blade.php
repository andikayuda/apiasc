@extends("crudbooster::admin_template")
@section("content")
<div class="card" style="background-color: white;padding : 20px;">
    <div class="card-body">

<div class="row">
    <div class="col-md-12 text-center" >
        <video controls width="200" height="400" src="{{URL::to($video->url)}}"></video>
    </div>
</div>
<div class="row">
    <div class="col-12 text-center">
        <p>Title : {{$video->title}}</p>
    </div>
</div>
<div class="row">
    <div class="col-12 text-center">
        <p>Description : {{$video->caption}}</p>
    </div>
</div>
<div class="row" >
    <div class="col-md-6 text-right">
        <button class="btn btn-primary" onclick="swal({
                                        title: 'Are you sure ?',
                                        type:'info',
                                        showCancelButton:true,
                                        allowOutsideClick:true,
                                        confirmButtonColor: '#367FA9',
                                        confirmButtonText: 'Confirm',
                                        cancelButtonText: 'Cancel',
                                        closeOnConfirm: false
                                        }, function(){
                                        
                                        location.href = 'https://backend-asc2021.yokesen.com/admin/confirm/<?php echo $video['id'] ?>';

                                        });">
            CONFIRM
        </button>
    </div>
    <div class="col-md-6 text-left" >
        <button class="btn btn-danger" data-toggle="modal" data-target="#exampleModalCenter">REJECT</button>
                <!-- Modal -->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Modal Reject</h5>
            </div>
            <form action="{{route('rejectVideo')}}" method="post">
                @csrf
            <div class="modal-body">
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Why you reject this video ?</label>
                    <textarea class="form-control" name="rejected_reason" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
                <input type="hidden" name="video_id" value="{{$video->id}}">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Reject</button>
            </div>
            </form>
            </div>
        </div>
        </div>
    </div>
</div>
</div>

</div>
@endsection