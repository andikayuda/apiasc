<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('admin');
});
Route::get('/admin/preview/{id}','pagesController@previewVideo')->middleware('isAdmin')->name('previewVideo');
Route::get('/admin/confirmed/{id}','pagesController@confirmedVideo')->middleware('isAdmin')->name('confirmedVideo');
Route::get('/admin/confirm/{id}','Api\videoController@accept')->middleware('isAdmin')->name('confirmVideo');
Route::post('/admin/reject','Api\videoController@reject')->middleware('isAdmin')->name('rejectVideo');