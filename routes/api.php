<?php

use Illuminate\Http\Request;
// use Response;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', 'Api\userController@login')->name('loginApi');
Route::get('login', 'Api\userController@tokenInvalid')->name('invalidToken');
Route::get('check-localstorage', 'Api\userController@tokenInvalid')->name('invalidToken');
Route::group(['middleware' => 'auth:api'], function(){
    Route::get('comment', 'Api\commentController@index');
    Route::post('comment', 'Api\commentController@store');
    Route::get('check-comment', 'Api\commentController@checkComment');
    Route::get('check-video', 'Api\videoController@checkVideo');
    // Route::post('video', 'Api\videoController@store');
    Route::get('video', 'Api\videoController@index');
    Route::get('user', 'Api\userController@userCheck');
    Route::post('logout', 'Api\userController@logout')->name('logout');
    Route::get('user/{id}', 'Api\userController@userById');
    Route::post('like', 'Api\videoLikesController@store');
    Route::post('video', 'Api\videoController@store');
    Route::get('user-video', 'Api\videoController@checkVideo');
    Route::get('get-all-user', 'Api\userController@getAllUser');
});


// Route::get('videos', function ()
// {
//     $filename = 'videos/1609144685-IMG20201205202645.jpg';
//     $path = storage_path($filename);
//     if (!File::exists($path)) {
//         abort(404);
//     }

//     $file = File::get($path);
//     $type = File::mimeType($path);

//     $response = Response::make($file, 200);
//     $response->header("Content-Type", $type);

//     return $response;
// });
