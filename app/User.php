<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use DB;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'nik', 'login',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function loginByNik($nik){
        return User::where('nik',$nik)->first();
    }

    public static function firstLogin($nik){
        $userId = User::loginByNik($nik)->id;
        $data = DB::table('login')->where('mainEvent','1')->where('user_id',$userId)->count();
        if($data!=1){
            return 'false';
        }
        else{
            return 'true';
        }
    }

    public static function userById($id){
        return User::where('id',$id)->first();
    }

}
