<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class comment extends Model
{
    //
    protected $table = 'comment';
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'comment', 'user_id',
    ];

    public static function getAllComment(){
        return comment::select('comment.id','users.name','users.nik','comment.comment','comment.created_at')->join('users','users.id','comment.user_id')->get();
    }

    public static function getUserComment($userId){
        $data = comment::where('user_id',$userId)->get();
        if($data->isNotEmpty()){
            return 'true';
        }
        return 'false';
    }

}
