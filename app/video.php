<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\videoLike;
class video extends Model
{
    //
    protected $table = 'videos';
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url', 'user_id','title','caption','rejected_reason', 'confirmed',
    ];

    public static function getAllVideo($userId){
        // return video::select('videos.id','users.name','users.nik','videos.url',DB::raw('CASE WHEN video-likes.video_id is null THEN "0" WHEN video-likes.video_id is not null THEN count("video_id") END as totalLike'),'videos.created_at')
        //         ->rightjoin('users','users.id','videos.user_id')
        //         ->leftJoin('video-likes','video-likes.video_id','videos.id')
        //         ->get();
        // return $userId;
                $data = videoLike::rightJoin('videos','video-likes.video_id','videos.id')
                                    ->leftJoin('users','users.id','videos.user_id');
                                    // ->leftJoin('video-likes','video-likes.video_id', 'videos.id');
                $data = $data->groupBy('videos.id');
                $data = $data->select(
                    'videos.id',
                    'users.name',
                    'users.nik',
                    'videos.url',
                    'videos.created_at',
                    // 'video-likes.user_id'
                    DB::raw('CASE WHEN `video-likes`.`video_id` is null THEN "0" WHEN `video-likes`.`video_id` is not null THEN count("video_id") END as totalLike'),
                    DB::raw('CASE WHEN `video-likes`.`user_id` != '.$userId.' THEN 0 WHEN `video-likes`.`user_id` = '.$userId.' THEN 1 END as userLike')
                );
                $data = $data->where('videos.confirmed','1');
                // $data = $data->where('video-likes.user_id',$userId);
                // $data =$data->select('videos.id');
                $data = $data->get();
                return $data;
    }

    public static function getUserVideo($userId){
        $data = video::where('user_id',$userId)->get();
        $video = video::where('user_id',$userId)->first();
        if($data->isEmpty()){
            return 'false';
        }
        else{
            $videoStatus = $video->confirmed;
            if($videoStatus == 0){
                return 'false';
            }
            return 'true';
        }
    }

    public static function userVideoStatus($userId){
        $userVideo = video::where('user_id',$userId)->get();
        $data = video::where('user_id',$userId)->orderBy('id','desc')->first();
        // return $data;
        $videoStatus = $data->confirmed;
                
        // return $data;
        if($userVideo->isEmpty()){
            return 'belum-upload';
        }
        // else{
        // return $videoStatus;
            if($videoStatus === 0){
                // $status = 'rejected';
                // return $data["confirmed"];
                return 'rejected';
            }
            elseif(is_null($videoStatus)){
                // $status = 'waiting-aproval';
                // return $data["confirmed"];
                return 'waiting-aproval';
            }
            elseif($videoStatus == 1){
                // $status = 'accepted';
                // return $data["confirmed"];
                return 'accepted';
            }
        // }
        // return $data;
    }

    public static function rejectedReason($userId){
        $userVideo = video::where('user_id',$userId)->get();
        $data = video::where('user_id',$userId)->orderBy('id','desc')->first();
        $rejectedReason = $data->rejected_reason;
        if($userVideo->isEmpty()){
            return NULL;
        }
        if(is_null($rejectedReason)){
            return NULL;
        }
        else{
            return $rejectedReason;
        }
    }

}
