<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class loginData extends Model
{
    //
    protected $table = 'login';
    public $timestamps = true;


    protected $guarded = [
        'id',
    ];
    
}
