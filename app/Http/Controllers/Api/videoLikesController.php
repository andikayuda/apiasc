<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\videoLike;
use VideoLikes;
use Illuminate\Support\Facades\Auth;

class videoLikesController extends Controller
{
    //
    public function store(Request $req){
        $validator = Validator::make($req->all(), [
            'user_id' => 'required',
            'video_id' => 'required',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        $videoLike = new VideoLike();
        $user = Auth::user();
        $userId = $user->id;
        $data = $videoLike->checkUserLike($userId, $req->video_id);
        $error["message"]="Cannot like video, You have already like a video before";


        if($data!=0){
            $data = $videoLike->deleteLike($userId, $req->video_id);
            if($data){
                $success['data'] = "unlike success";
                return response()->json(['success' => $success], 200);
            }
            else{
                return response()->json(['error'=>'Unlike Failed'], 401);
            }
            return response()->json(['error' => $error], 410);
        }
        else{

            $data = $videoLike->createLike($userId, $req->video_id);
            if($data){
                $success['data'] = $data;
                return response()->json(['success' => $success], 200);
            }
            else{
                return response()->json(['error'=>'Insert Failed'], 401);
            }
        }
    }
}
