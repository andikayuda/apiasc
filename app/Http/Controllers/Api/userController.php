<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\loginData;

class userController extends Controller
{
    public $successStatus = 200;
    

    public function login(Request $req){
         $validator = Validator::make($req->all(), [
            'nik' => 'required',
            'mainEvent' => 'required',
        ]);
        
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }

        

        $user = User::loginByNik($req->nik);
        
        if(!$user){
            return response()->json(['error'=>'Login Failed'], 401);
        }
        elseif($user->login == 1){
            return response()->json(['error'=>'Cannot Login, You have already login'], 401);
        }
        else{
            $user->login = 1;
            $user->save();
            $loginData = new loginData();
            $loginData->user_id = $user->id;
            $loginData->ipAddress = $req->ip();
            $loginData->mainEvent = $req->mainEvent;
            $loginData->browser = NULL;
            $loginData->save();
            // $user->token()->revoke();
            $success['token'] =  $user->createToken('ASC')->accessToken;
            $success['users'] = $user;
            $success['firstLogin'] = User::firstLogin($req->nik);
            return response()->json(['success' => $success], $this->successStatus);
        }

        // $user = User::loginByNik($nik);
        
        // if(!$user){
        //     return response()->json(['error'=>'Login Failed'], 401);
        // }
        // elseif($user->login == 1){
        //     return response()->json(['error'=>'Cannot Login, You have already login'], 401);
        // }
        // else{
        //     $user->login = 1;
        //     $user->save();
        //     // $user->token()->revoke();
        //     $success['token'] =  $user->createToken('ASC')->accessToken;
        //     $success['users'] = $user;
        //     $success['firstLogin'] = User::firstLogin($nik);
        //     return response()->json(['success' => $success], $this->successStatus);
        // }
    }

    public function user(){
        return response()->json('cek');
    }

    public function userById($id){
        $success['users'] = User::userById($id);
        $success['firstLogin'] = User::firstLogin($success['users']->nik);
        return response()->json(['success' => $success], $this->successStatus);
    }

    public function userCheck(){
        $user = Auth::user();
        return $user;
    }

    public function logout(){
        // if(Auth::user())
        $user = Auth::user();
        $user->login = 0;
        $user->save();
        $user->token()->revoke();
        return response()->json(['Success'=>'Logout Success'], 200);
    }

    public function tokenInvalid(){
        return response()->json(['error'=>'Invalid Token'], 200);
    }

    public function getAllUser(){
        $data = User::select('users.id','users.nik','users.name','users.email','comment.comment','commitmentPhoto.photo')
                ->join('commitmentPhoto','commitmentPhoto.user_id','users.id')
                ->leftJoin('comment','comment.user_id','users.id')->get();
        return response()->json(['success' => $data], $this->successStatus);
    }
}
