<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\comment;
use Validator;
use Illuminate\Support\Facades\Auth;

class commentController extends Controller
{
    //
    public $successStatus = 200;

    public function index(){
       
        $success['comment'] = comment::getAllComment();
        return response()->json(['success' => $success], $this->successStatus);
    }

    public function store(Request $req){
       
        $validator = Validator::make($req->all(), [
            'user_id' => 'required',
            'comment' => 'required',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        
        $data = comment::create([
            'user_id' => $req->user_id,
            'comment' => $req->comment,
        ]);
        if($data){
            $success['data'] = $data;
            return response()->json(['success' => $success], $this->successStatus);
        }
        else{
            return response()->json(['error'=>'Insert Failed'], 401);
        }
    }

    public function checkComment(){
        $user = Auth::user();
        $userId = $user->id;
        $success['comment'] = comment::getUserComment($userId);
        return response()->json(['success' => $success], $this->successStatus);
    }
}
