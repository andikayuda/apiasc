<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\video;
use Storage;
use Validator;
use Illuminate\Support\Facades\Auth;
use VideoThumbnail;

class videoController extends Controller
{
    //
    public $successStatus = 200;
    
    public function index(){
        $user = Auth::user();
        $userId = $user->id;
        $success['video'] = video::getAllVideo($userId);
        return response()->json(['success' => $success], $this->successStatus);
    }
    public function store(Request $request)
    {
        // dd(phpinfo());
        $validator = Validator::make(
            $request->all(),
            [
                'title' => 'required|max:70',
                'caption' => 'required|max:160',
                'user_id' => 'required',
                'file' => 'required|mimes:mp4,mkv,avi,mpeg,wmp,3gp|max:35000',
            ]
        );


        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $user = Auth::user();
        $userId = $user->id;

        $checkUserVideo = video::getUserVideo($userId);
        if($checkUserVideo == "true"){
            $error['message'] = "Failed to upload video, you have already uploaded a video before";
            return response()->json(['error' => $error], 401);
        }
 
        if ($files = $request->file('file')) {

            //store file into document folder
            $thumbnail = VideoThumbnail::createThumbnail($files, public_path('thumbnail/'), 'movie.jpg', 2, 1920, 1080);
            // return 'thumbnail success';
            date_default_timezone_set("Asia/Jakarta");
            $file = $request->file('file');
            
            $ffprobe = \FFMpeg\FFProbe::create();
            
            $videoDuration = $ffprobe->format($file) // extracts file informations
                    ->get('duration');             // returns the duration property

            if($videoDuration > 65){
                $error['message'] = "Video duration more than 1 minute";
                return response()->json(['error' => $error], 401);
            }
            
            $uniqueFileName = str_replace(" ","",time()."-".$file->getClientOriginalName());
            // return $uniqueFileName;
            // $file->storeAs('uploads/', $uniqueFileName);
            // Storage::put('uploads/', $uniqueFileName);
            
            $path = $file->storeAs('videos', $uniqueFileName);
            // $path = $file->storeAs('/storage/uploads/', $uniqueFileName);

            
            // $files = Storage::allFiles('uploads/'.$uniqueFileName);

            //store your file into database
            $video = new video();
            $video->url = "storage/".$path;
            $video->user_id = $userId;
            $video->title = $request->title;
            $video->caption = $request->caption;
            $video->rejected_reason = NULL;
            $video->confirmed = NULL;
            $data = $video->save();

            return response()->json([
                "success" => true,
                "message" => "File successfully uploaded",
                "file" => $file
            ]);
        }
    }

    public function checkVideo(){
        $user = Auth::user();
        $userId = $user->id;
        $data = video::userVideoStatus($userId);
        $reason = video::rejectedReason($userId);
        
        // return $data;
        // if($data->isEmpty()){
        //     return 'belum upload';
        // }
        // else{
        //     if($data["confirmed"] == 0){
        //         $status = 'rejected';
        //         // return $data["confirmed"];
        //         // return 'rejected';
        //     }
        //     elseif(is_null($data["confirmed"])){
        //         $status = 'waiting-aproval';
        //         // return $data["confirmed"];
        //         // return 'waiting-aproval';
        //     }
        //     elseif($data["confirmed"] == 1){
        //         $status = 'accepted';
        //         // return $data["confirmed"];
        //         // return 'accepted';
        //     }
        // }
        $success['videoStatus'] = $data;
        $success['rejectedReason'] = $reason;
        return response()->json(['success' => $success], $this->successStatus);
    }

    public function accept($id){
        $video = video::find($id);
        $video->confirmed = '1';
        $data = $video->save();
        // return response()->json([
        //     "success" => true,
        //     "message" => "Video successfully confirmed",
        //     "file" => $data
        // ], 200);
        return redirect('/admin/videos');
    }

    public function reject(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'rejected_reason' => 'required|max:70',
                'video_id' => 'required'
            ]
        );


        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $video = video::find($request->video_id);
        $video->confirmed = 0;
        $video->rejected_reason = $request->rejected_reason;
        $data = $video->save();
        // return response()->json([
        //     "success" => true,
        //     "message" => "Video successfully rejected",
        //     "file" => $data
        // ], 200);
        return redirect('/admin/videos');
    } 
}
