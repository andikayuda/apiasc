<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\video;
class pagesController extends Controller
{
    //
    public function previewVideo($id){
        $video = video::find($id);
        return view('admin.preview-video',compact('video'));
    }
    public function confirmedVideo($id){
        $video = video::find($id);
        return view('admin.confirmed-video',compact('video'));
    }
}
