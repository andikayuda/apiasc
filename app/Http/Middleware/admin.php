<?php

namespace App\Http\Middleware;

use Closure;
use CRUDBooster;

class admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(CRUDBooster::myPrivilegeId()=='1'||CRUDBooster::myPrivilegeId()=='2'){
            return $next($request);
        }else{
            return redirect('admin/login');
        }
    }
}
