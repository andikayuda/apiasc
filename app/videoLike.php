<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class videoLike extends Model
{
    //
    protected $table = 'video-likes';
    public $timestamps = true;

    public $successStatus = 200;
    public $errorStatus = 401;

    protected $fillable = [
        'user_id', 'video_id',
    ];

    public static function createLike($user_id, $video_id){
        $input = videoLike::create([
            'user_id' => $user_id,
            'video_id' => $video_id
          ]);
        return $input;
    }
    public static function deleteLike($userId, $video_id){
        $data = videoLike::where('user_id',$userId)->where('video_id',$video_id)->delete();
        return 'unlike success';

    }

    public function checkUserLike($user_id, $video_id){
        $data = videoLike::where('user_id',$user_id)->where('video_id', $video_id)->count();
        return $data;
    }
}
