<?php

use Illuminate\Database\Seeder;

class insertCommitmentFoto extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $angka = 01;
        $datas = \DB::table('commitmentPhoto')->get();
        foreach($datas as $data){
            if($angka<100){
                $url = 'storage/commitment_picture/cut_images_YUZNMRqR6WGuj/image_part_0';
            }
            else{
                $url = 'storage/commitment_picture/cut_images_YUZNMRqR6WGuj/image_part_';
            }
            \DB::table('commitmentPhoto')
              ->where('id', $data->id)
              ->update(['photo' => $url.$angka.'.jpg']);
            $angka++;
        }
    }
}
