<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $nik = 20000;
        for($i=1;$i<159;$i++){
            \App\User::create([
                'nik' => 'NIK-'.$nik,
                'name'	=> str_random(20),
                'email'	=> str_random(10). '@admin.com'
                ]);
            $nik++;
        }

    }
}
